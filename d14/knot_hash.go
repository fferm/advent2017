package d14

/*import (
    "fmt"
)*/
var addon = []byte {17, 31, 73, 47, 23}
var skipSize int
var data []byte
var lengths []byte
var currentPosition byte

func knotHashOf(input string) [16]byte {
    skipSize = 0
    currentPosition = byte(0)

    fillData()
    fixLengths(input)

    for i := 0; i < 64; i++ {
        transmogrify()
    }

    return reduce()
}

func reduce() [16]byte {
    var denseHash [16]byte

    for denseIdx := 0; denseIdx < 16; denseIdx++ {
        sparseChar := byte(0)
        for i := 0; i < 16; i++ {
            sparseIdx := denseIdx * 16 + i
            sparseChar = sparseChar ^ data[sparseIdx]
        }
        denseHash[denseIdx] = sparseChar
    }

    return denseHash;
}

func transmogrify() {
    for i := 0; i < len(lengths); i++ {
        length := lengths[i]

        var sublist []byte
        for j := byte(0); j < length ; j++ {
            sublist = append(sublist, data[currentPosition + j])
        }
        for j := byte(0); j < byte(len(sublist)); j++ {
            dataIdx := currentPosition + length - j - byte(1)
            data[dataIdx] = sublist[j]
        }

        currentPosition = byte(int(currentPosition) + int(length) + skipSize)
        skipSize++
    }
}

func fixLengths(input string) {
    lengths = []byte(input)
    for _, b := range addon {
        lengths = append(lengths, b)
    }
}

func fillData() {
    data = []byte{}
    for i := 0; i < 256; i++ {
        data = append(data, byte(i))
    }
}