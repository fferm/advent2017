package d14

import (
    "fmt"
)

//var input = "flqrgnkx"
var input = "ffayrhll"
var rowCount = 128

type coord struct{
    x int
    y int
}

//var oneCoords map[coord]bool    // bool visar om jag varit där

func D14() {
    oneCoords := fillOneCoords()

    analyzeGroups(oneCoords)
}

func fillOneCoords() map[coord]bool {
    oneCoords := make(map[coord]bool)

    for row := 0; row < rowCount; row++ {
        hashInput := fmt.Sprintf("%s-%d", input, row)
        knotHash := knotHashOf(hashInput)

        for knotHashIdx, workingKnotHash := range knotHash {
/*            if knotHashIdx != 0 {
                continue
            }*/

            for i := 7; i >= 0; i-- {
                if (workingKnotHash & byte(1)) == byte(1) {
                    c := coord{knotHashIdx * 8 + i, row}
                    oneCoords[c] = false
                }
                workingKnotHash = workingKnotHash >> 1
            }
        }

//        fmt.Printf("%08b\n", knotHash[0])
    }

    return oneCoords

}

func howManyOnes(input byte) int {
    result := 0
    work := input
    for i := 0 ; i < 8; i++ {
        if (work & byte(1)) == byte(1) {
            result ++
        }
        work = work >> 1
    }
    return result;
}

func analyzeGroups(oneCoords map[coord]bool) {
    numGroups := 0
    for c, visited := range oneCoords {
        if visited {
//            fmt.Printf("Visited: %d\n", c)
            continue
        }

        visitCoordinate(c, &oneCoords)
        numGroups++
    }

    fmt.Printf("%d groups\n", numGroups)
}

func visitCoordinate(c coord, oneCoordsPtr *map[coord]bool) {
    _, exists := (*oneCoordsPtr)[c]
    if !exists {
        return
    }

    if ((*oneCoordsPtr)[c]) {
        return
    }

//    fmt.Printf("Visiting coord: %d\n", c)
    (*oneCoordsPtr)[c] = true

    visitCoordinate(coord{c.x + 1, c.y}, oneCoordsPtr)
    visitCoordinate(coord{c.x - 1, c.y}, oneCoordsPtr)
    visitCoordinate(coord{c.x, c.y + 1}, oneCoordsPtr)
    visitCoordinate(coord{c.x, c.y - 1}, oneCoordsPtr)
}