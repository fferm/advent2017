package d22

import (
	"fmt"
	"io/ioutil"
	"strings"
)

var numIterations = 10000000

//var numIterations = 10000

//var numIterations = 70

//var numIterations = 100

var filename = "d22/d22_input"

//var filename = "d22/d22_input_small"

type coord struct {
	row, col int
}

type state int

const (
	clean state = iota
	weakended
	infected
	flagged
)

type board struct {
	currentPosition, currentDirection coord
	maxRow, minRow, maxCol, minCol    int
	infectedNodesPtr                  *map[coord]state
}

var infectionCounter = 0

func D22() {
	boardPtr := getInput()
	fmt.Printf("Start position \n%s\n", boardPtr)

	for iteration := 0; iteration < numIterations; iteration++ {
		iterate(boardPtr)
		//fmt.Printf("After iteration %d\n%s\n", iteration+1, boardPtr)
	}

	fmt.Printf("%d iterations caused an infection\n", infectionCounter)
}

func iterate(boardPtr *board) {
	currentPosition := (*boardPtr).currentPosition
	currentPositionState, currentPositionHasState := (*(*boardPtr).infectedNodesPtr)[currentPosition]

	if !currentPositionHasState {
		currentPositionState = clean
	}

	switch currentPositionState {
	case clean:
		infectCurrentPosition(boardPtr, weakended)
		turnLeft(boardPtr)
	case weakended:
		infectCurrentPosition(boardPtr, infected)
	case infected:
		infectCurrentPosition(boardPtr, flagged)
		turnRight(boardPtr)
	case flagged:
		cleanCurrentPosition(boardPtr)
		turnLeft(boardPtr)
		turnLeft(boardPtr)
	}
	moveForward(boardPtr)
}

func turnRight(boardPtr *board) {
	oldRow := (*boardPtr).currentDirection.row
	oldCol := (*boardPtr).currentDirection.col

	/*    (-1, 0) -> (0, 1)
	      (0, 1) -> (1, 0)
	      (1, 0) -> (0, -1)
	      (0, -1) -> (-1, 0)*/

	newRow := oldCol
	newCol := -oldRow

	(*boardPtr).currentDirection = coord{row: newRow, col: newCol}

}
func turnLeft(boardPtr *board) {
	oldRow := (*boardPtr).currentDirection.row
	oldCol := (*boardPtr).currentDirection.col

	/*    (-1, 0) -> (0, -1)
	      (0, -1) -> (1, 0)
	      (1, 0) -> (0, 1)
	      (0, 1) -> (-1, 0)*/

	newRow := -oldCol
	newCol := oldRow

	(*boardPtr).currentDirection = coord{row: newRow, col: newCol}
}
func cleanCurrentPosition(boardPtr *board) {
	delete((*(*boardPtr).infectedNodesPtr), (*boardPtr).currentPosition)
}
func infectCurrentPosition(boardPtr *board, state state) {
	(*(*boardPtr).infectedNodesPtr)[(*boardPtr).currentPosition] = state
	if state == infected {
		infectionCounter++
	}
}
func moveForward(boardPtr *board) {
	(*boardPtr).currentPosition.add((*boardPtr).currentDirection)
	newRow := (*boardPtr).currentPosition.row
	newCol := (*boardPtr).currentPosition.col

	if newRow > (*boardPtr).maxRow {
		(*boardPtr).maxRow = newRow
	}
	if newRow < (*boardPtr).minRow {
		(*boardPtr).minRow = newRow
	}
	if newCol > (*boardPtr).maxCol {
		(*boardPtr).maxCol = newCol
	}
	if newCol < (*boardPtr).minCol {
		(*boardPtr).minCol = newCol
	}
}

func (c *coord) add(other coord) {
	c.row += other.row
	c.col += other.col
}

func getInput() (boardPtr *board) {
	var rowsInFile []string
	fullFile, _ := ioutil.ReadFile(filename)
	for _, s := range strings.Split(string(fullFile), "\n") {
		trimmed := strings.Trim(s, "\r")
		rowsInFile = append(rowsInFile, trimmed)
	}

	infectedNodes := make(map[coord]state)
	b := board{infectedNodesPtr: &infectedNodes}

	maxCol := 0
	maxRow := 0
	for rowNum, row := range rowsInFile {
		if len(row) > maxCol {
			maxCol = len(row)
		}
		if rowNum > maxRow {
			maxRow = rowNum
		}

		for colNum, c := range row {
			if string(c) == "#" {
				infectedNodes[coord{row: rowNum, col: colNum}] = infected
			}
		}
	}

	b.minRow = 0
	b.minCol = 0
	b.maxRow = maxRow + 1
	b.maxCol = maxCol
	b.currentPosition = coord{row: (b.maxRow - b.minRow) / 2, col: (b.maxCol - b.minCol) / 2}
	b.currentDirection = coord{row: -1, col: 0}

	return &b

}

func (b *board) String() string {
	ret := fmt.Sprintf("Direction: (%d, %d)\n", b.currentDirection.row, b.currentDirection.col)
	for row := b.minRow; row <= b.maxRow; row++ {
		for col := b.minCol; col <= b.maxCol; col++ {

			var fmtString string
			if b.currentPosition.row == row && b.currentPosition.col == col {
				fmtString = "[%s]"
			} else {
				fmtString = " %s "
			}
			var nodeString string
			state, infectedNode := (*b.infectedNodesPtr)[coord{row: row, col: col}]
			if infectedNode {
				if state == infected {
					nodeString = "#"
				} else if state == weakended {
					nodeString = "W"
				} else if state == flagged {
					nodeString = "F"
				} else if state == clean {
					nodeString = ","
				}
			} else {
				nodeString = "."
			}

			ret += fmt.Sprintf(fmtString, nodeString)
		}
		ret += "\n"
	}
	return ret
}
