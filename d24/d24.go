package d24

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

//var filename = "d24/d24_input_smaller"

//var filename = "d24/d24_input_small"

var filename = "d24/d24_input"

type component struct {
	i1, i2, id int
}

type bridge struct {
	components []component
	end        int
}

var validBridges []bridge
var allComponents []component

func D24() {
	allComponents = getInput()

	startingPoint := 0

	componentsFromStartingPoint := findComponentsFrom(startingPoint)

	for _, s := range componentsFromStartingPoint {
		var end int
		if s.i1 == 0 {
			end = s.i2
		} else {
			end = s.i1
		}
		b := bridge{
			components: []component{s},
			end:        end,
		}
		buildBridgeFrom(b)
	}

	maxStrength := 0
	maxLength := 0
	for _, b := range validBridges {
		fmt.Printf("%s\n", b)
		if len(b.components) >= maxLength {
			maxLength = len(b.components)
			if b.strength() > maxStrength {
				maxStrength = b.strength()
			}
		}
	}
	fmt.Printf("Max strength: %d with length: %d\n", maxStrength, maxLength)
	fmt.Printf("%d bridges created\n", len(validBridges))
}

func buildBridgeFrom(b bridge) {
	validBridges = append(validBridges, b)

	additionalComponents := findComponentsFrom(b.end)
	for _, c := range additionalComponents {
		var end int
		if c.i1 == b.end {
			end = c.i2
		} else {
			end = c.i1
		}
		if !b.hasComponent(c) {
			newComponents := []component{}
			for _, oldComp := range b.components {
				newComponents = append(newComponents, oldComp)
			}
			newComponents = append(newComponents, c)

			newBridge := bridge{
				components: newComponents,
				end:        end,
			}
			buildBridgeFrom(newBridge)
		}
	}
}

func (b bridge) hasComponent(c component) bool {
	for _, bridgeComponent := range b.components {
		if c.id == bridgeComponent.id {
			return true
		}
	}
	return false
}

func findComponentsFrom(startingPoint int) []component {
	ret := []component{}
	for _, c := range allComponents {
		if c.i1 == startingPoint || c.i2 == startingPoint {
			ret = append(ret, c)
		}
	}
	return ret
}

func (c component) String() string {
	return fmt.Sprintf("%d/%d", c.i1, c.i2)
}

func (b bridge) String() string {
	ret := ""
	for _, c := range b.components {
		ret += fmt.Sprintf("%s", c)
		ret += " -> "
	}

	ret += fmt.Sprintf("\t\tstrength: %d\n", b.strength())

	return ret
}

func (c component) strength() int {
	return c.i1 + c.i2
}

func (b bridge) strength() int {
	ret := 0
	for _, c := range b.components {
		ret += c.strength()
	}

	return ret
}

func getInput() []component {
	ret := []component{}
	fullFile, _ := ioutil.ReadFile(filename)
	for id, s := range strings.Split(string(fullFile), "\n") {
		trimmed := strings.Trim(s, "\r")
		trimmed = strings.Trim(trimmed, " ")
		if trimmed == "" {
			continue
		}

		parts := strings.Split(trimmed, "/")

		i1, _ := strconv.Atoi(parts[0])
		i2, _ := strconv.Atoi(parts[1])

		c := component{i1: i1, i2: i2, id: id}
		ret = append(ret, c)
	}
	return ret
}
