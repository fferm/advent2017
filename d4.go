package main

import (
	"fmt"
	"io/ioutil"
	"strings"
)

var input []string

func d4() {
	input = getInput4()

	result := 0
	for i := 0; i < len(input); i++ {
		row := input[i]

		words := strings.Split(row, " ")
		if !hasDuplicates(words) {
			result++
		}
	}
	fmt.Printf("%d rader har ej dubletter \n", result)
}

func getInput4() []string {
	fullFile, _ := ioutil.ReadFile("d4_input")

	split := strings.Split(string(fullFile), "\n")

	return split;
}

func hasDuplicates(s []string) bool {
	for i := 0; i < len(s); i++ {
		word1:= strings.Trim(s[i], string(13))

		for j := i+1; j < len(s); j++ {
			word2 := strings.Trim(s[j], string(13))
			if isAnagramOf(word1, word2) {
				return true
			}
		}
	}
	return false
}

func isAnagramOf(s1 string, s2 string) bool {
	if len(s1) != len(s2) {
		return false;
	}

	for i := 0; i < len(s1); i++ {
		if !strings.Contains(s2, string(s1[i])) {
			return false;
		}
	}

	return true
}