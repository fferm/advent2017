package d21

import (
	"fmt"
	"io/ioutil"
	"strings"
)

type coord struct {
	row, col int
}

type square struct {
	size  int
	posOn *map[coord]interface{}
}

type board struct {
	numSquaresOnSide int // koordinatsystemets enheter är antal squares, inte antal positioner
	squareSize       int
	squares          *map[coord]*square
}

type rule struct {
	appliesToSquareSize int
	inputSquare         square
	output00            square
	output01            square // Ej relevant för appliesToSquareSize = 2
	output10            square // Ej relevant för appliesToSquareSize = 2
	output11            square // Ej relevant för appliesToSquareSize = 2
}

//var numIterations = 2

var numIterations = 18

var filename = "d21/d21_input"

//var filename = "d21/d21_input_small"

func D21() {
	b := createStartBoard()
	boardPtr := &b

	rules := getInput()

	fmt.Println("Start board")
	fmt.Println(*boardPtr)
	for iteration := 1; iteration <= numIterations; iteration++ {
		boardPtr.change(rules)
		boardPtr = boardPtr.reformat()
		fmt.Printf("After iteration %d  Board is now %d x %d = %d squares of size %d\n", iteration, (*boardPtr).numSquaresOnSide, (*boardPtr).numSquaresOnSide, (*boardPtr).numSquaresOnSide*(*boardPtr).numSquaresOnSide, (*boardPtr).squareSize)
		//fmt.Println(board)
	}
	//	fmt.Printf("Final board\n%s\n", *boardPtr)
	fmt.Printf("%d pixels are on\n", (*boardPtr).numPixelsOn())
}
func createStartBoard() board {
	posOn := make(map[coord]interface{})
	posOn[coord{row: 0, col: 1}] = true // Value is uninteresting
	posOn[coord{row: 1, col: 2}] = true // Value is uninteresting
	posOn[coord{row: 2, col: 0}] = true // Value is uninteresting
	posOn[coord{row: 2, col: 1}] = true // Value is uninteresting
	posOn[coord{row: 2, col: 2}] = true // Value is uninteresting

	sq := square{
		size:  3,
		posOn: &posOn,
	}

	s := make(map[coord]*square)
	s[coord{row: 0, col: 0}] = &sq

	b := board{
		numSquaresOnSide: 1,
		squareSize:       3,
		squares:          &s,
	}
	return b
}

func (b *board) reformat() *board {
	if b.squareSize == 2 {
		return b
	}

	totalSize := b.squareSize * b.numSquaresOnSide
	if totalSize%2 != 0 {
		return b
	}

	fmt.Printf("Reformat needed for \n%s\n", b)

	newNumSquaresOnSide := b.numSquaresOnSide * 3 / 2

	s := make(map[coord]*square)
	for newBoardRow := 0; newBoardRow < newNumSquaresOnSide; newBoardRow++ {
		for newBoardCol := 0; newBoardCol < newNumSquaresOnSide; newBoardCol++ {
			p := make(map[coord]interface{})
			sq := square{
				size:  2,
				posOn: &p,
			}
			s[coord{row: newBoardRow, col: newBoardCol}] = &sq
		}
	}
	newBoard := board{
		numSquaresOnSide: newNumSquaresOnSide,
		squareSize:       2,
		squares:          &s,
	}

	fmt.Printf("Created new board\n")

	for totalRow := 0; totalRow < totalSize; totalRow++ {
		newBoardRow := totalRow / 2
		newSquareRow := totalRow % 2

		for totalCol := 0; totalCol < totalSize; totalCol++ {
			newBoardCol := totalCol / 2
			newSquareCol := totalCol % 2

			if b.isPixelOn(totalRow, totalCol) {
				squarePtr := (*newBoard.squares)[coord{row: newBoardRow, col: newBoardCol}]
				(*squarePtr.posOn)[coord{row: newSquareRow, col: newSquareCol}] = true
			}
		}
	}

	fmt.Printf("Reformat completed\n")

	return &newBoard
}

func (b board) isPixelOn(totalRow, totalCol int) bool {
	boardRow := totalRow / b.squareSize
	boardCol := totalCol / b.squareSize
	boardCoord := coord{row: boardRow, col: boardCol}

	squareRow := totalRow % b.squareSize
	squareCol := totalCol % b.squareSize
	squareCoord := coord{row: squareRow, col: squareCol}

	s := (*(*b.squares)[boardCoord])
	_, isOn := (*s.posOn)[squareCoord]

	return isOn
}

func (b *board) change(rules []rule) {
	fmt.Println("Change started")
	// Glesa ut först om det behövs
	if b.squareSize == 3 {
		fmt.Println("Putting in new squares")
		for boardRow := b.numSquaresOnSide - 1; boardRow >= 0; boardRow-- {
			for boardCol := b.numSquaresOnSide - 1; boardCol >= 0; boardCol-- {
				//				fmt.Printf("Counting down: r: %d, c. %d\n", boardRow, boardCol)
				boardCoord := coord{row: boardRow, col: boardCol}
				currentSquarePtr := (*b.squares)[boardCoord]

				delete((*b.squares), boardCoord)

				newBoardCoord := coord{row: boardRow * 2, col: boardCol * 2}
				(*b.squares)[newBoardCoord] = currentSquarePtr
			}
		}
		b.numSquaresOnSide *= 2
	}

	//	fmt.Printf("Efter glesning: \n%s\n", b)

	// Gå igenom alla som är kvar
	var boardStep int
	if b.squareSize == 3 {
		boardStep = 2
	} else {
		boardStep = 1
	}
	fmt.Println("Starting change of squares")
	ctr := 0
	for boardRow := 0; boardRow < b.numSquaresOnSide; boardRow += boardStep {
		for boardCol := 0; boardCol < b.numSquaresOnSide; boardCol += boardStep {
			ctr++
			if ctr%10000 == 0 {
				fmt.Printf("%d steps of %d\n", ctr, b.numSquaresOnSide*b.numSquaresOnSide)
			}
			boardCoord := coord{row: boardRow, col: boardCol}

			currentSquarePtr := (*b.squares)[boardCoord]
			if currentSquarePtr == nil {
				fmt.Printf("Nil pointer for board coodinates: (%d, %d)\n", boardRow, boardCol)
				continue
			}

			//			fmt.Printf("BoardCoord: (%d, %d).  Has Square:\n%s\n", boardCoord.row, boardCoord.col, *currentSquarePtr)

			matchingRule := findMatchingRule(rules, currentSquarePtr)
			//fmt.Printf("Square: \n%s\nMatching rule: \n%s\n", *currentSquarePtr, matchingRule)
			delete((*b.squares), boardCoord)

			(*b.squares)[boardCoord] = &matchingRule.output00
			if b.squareSize == 2 {
				//				b.squareSize = 3
			} else { // squareSize = 3
				(*b.squares)[coord{row: boardCoord.row, col: boardCoord.col + 1}] = &matchingRule.output01
				(*b.squares)[coord{row: boardCoord.row + 1, col: boardCoord.col}] = &matchingRule.output10
				(*b.squares)[coord{row: boardCoord.row + 1, col: boardCoord.col + 1}] = &matchingRule.output11
				//				b.squareSize = 2
			}
		}
	}
	if b.squareSize == 3 {
		b.squareSize = 2
	} else {
		b.squareSize = 3
	}
	fmt.Println("Change completed")
}

func getInput() []rule {
	rules := []rule{}
	fullFile, _ := ioutil.ReadFile(filename)
	for _, s := range strings.Split(string(fullFile), "\n") {
		trimmed := strings.Trim(s, "\r")

		parts := strings.Split(trimmed, "=>")

		inputString := strings.Trim(parts[0], " ")
		outputString := strings.Trim(parts[1], " ")

		var r rule
		if len(inputString) == 5 {
			input := readSize2(inputString)
			output := readSize3(outputString)
			r = rule{
				appliesToSquareSize: 2,
				inputSquare:         input,
				output00:            output,
			}
		} else if len(inputString) == 11 {
			input := readSize3(inputString)
			o00, o01, o10, o11 := readSize4(outputString)
			r = rule{
				appliesToSquareSize: 3,
				inputSquare:         input,
				output00:            o00,
				output01:            o01,
				output10:            o10,
				output11:            o11,
			}
		} else {
			fmt.Printf("Unknown input string %s\n", inputString)
		}

		rules = append(rules, r)

	}

	return rules

}

func (bPtr *board) numPixelsOn() int {
	ret := 0
	for _, squarePtr := range *(*bPtr).squares {
		posOn := *squarePtr.posOn
		ret += len(posOn)
	}

	return ret
}

func findMatchingRule(rules []rule, squarePtr *square) rule {
	//fmt.Printf("Find matching rule for \n%s\n", *squarePtr)
	for _, ru := range rules {
		if ru.matches(*squarePtr) {
			return ru
		}
	}
	fmt.Printf("Did not find matching rule for square\n%s", *squarePtr)
	return rules[0]
}

func (r rule) matches(s square) bool {
	if r.appliesToSquareSize != s.size {
		return false
	}

	squareToTest := r.inputSquare
	for i := 0; i < 4; i++ {
		//		fmt.Printf("SquareToTest\n%s", squareToTest)
		if squareToTest.matchesWithoutTransform(s) {
			//fmt.Printf("Matching rule \n%s\n", r)
			return true
		}
		squareToTest = squareToTest.rotate()
	}
	squareToTest = squareToTest.flip()
	for i := 0; i < 4; i++ {
		//		fmt.Printf("SquareToTest\n%s", squareToTest)
		if squareToTest.matchesWithoutTransform(s) {
			//fmt.Printf("Matching rule \n%s\n", r)
			return true
		}
		squareToTest = squareToTest.rotate()
	}

	return false
}

func (s1 square) matchesWithoutTransform(s2 square) bool {
	if s1.size != s2.size {
		return false
	}
	for r := 0; r <= s1.size; r++ {
		for c := 0; c < s1.size; c++ {
			co := coord{row: r, col: c}

			_, inS1 := (*s1.posOn)[co]
			_, inS2 := (*s2.posOn)[co]

			if inS1 != inS2 {
				return false
			}
		}
	}
	return true
}

func (s square) rotate() square {
	pos := make(map[coord]interface{})
	ret := square{
		size:  s.size,
		posOn: &pos,
	}
	for c, _ := range *s.posOn {
		(*ret.posOn)[coord{row: c.col, col: s.size - 1 - c.row}] = true
	}
	return ret
}

func (s square) flip() square {
	pos := make(map[coord]interface{})
	ret := square{
		size:  s.size,
		posOn: &pos,
	}
	for c, _ := range *s.posOn {
		(*ret.posOn)[coord{row: c.col, col: c.row}] = true
	}
	return ret
}

func readSize2(inputString string) square {
	p := make(map[coord]interface{})

	if string(inputString[0]) == "#" {
		p[coord{row: 0, col: 0}] = true
	}
	if string(inputString[1]) == "#" {
		p[coord{row: 0, col: 1}] = true
	}
	if string(inputString[3]) == "#" {
		p[coord{row: 1, col: 0}] = true
	}
	if string(inputString[4]) == "#" {
		p[coord{row: 1, col: 1}] = true
	}

	s := square{
		size:  2,
		posOn: &p,
	}

	return s
}

func readSize3(inputString string) square {
	p := make(map[coord]interface{})

	if string(inputString[0]) == "#" {
		p[coord{row: 0, col: 0}] = true
	}
	if string(inputString[1]) == "#" {
		p[coord{row: 0, col: 1}] = true
	}
	if string(inputString[2]) == "#" {
		p[coord{row: 0, col: 2}] = true
	}
	if string(inputString[4]) == "#" {
		p[coord{row: 1, col: 0}] = true
	}
	if string(inputString[5]) == "#" {
		p[coord{row: 1, col: 1}] = true
	}
	if string(inputString[6]) == "#" {
		p[coord{row: 1, col: 2}] = true
	}
	if string(inputString[8]) == "#" {
		p[coord{row: 2, col: 0}] = true
	}
	if string(inputString[9]) == "#" {
		p[coord{row: 2, col: 1}] = true
	}
	if string(inputString[10]) == "#" {
		p[coord{row: 2, col: 2}] = true
	}

	s := square{
		size:  3,
		posOn: &p,
	}

	return s
}

func readSize4(inputString string) (o00, o01, o10, o11 square) {
	p00 := make(map[coord]interface{})
	p01 := make(map[coord]interface{})
	p10 := make(map[coord]interface{})
	p11 := make(map[coord]interface{})

	if string(inputString[0]) == "#" {
		p00[coord{row: 0, col: 0}] = true
	}
	if string(inputString[1]) == "#" {
		p00[coord{row: 0, col: 1}] = true
	}
	if string(inputString[5]) == "#" {
		p00[coord{row: 1, col: 0}] = true
	}
	if string(inputString[6]) == "#" {
		p00[coord{row: 1, col: 1}] = true
	}

	if string(inputString[2]) == "#" {
		p01[coord{row: 0, col: 0}] = true
	}
	if string(inputString[3]) == "#" {
		p01[coord{row: 0, col: 1}] = true
	}
	if string(inputString[7]) == "#" {
		p01[coord{row: 1, col: 0}] = true
	}
	if string(inputString[8]) == "#" {
		p01[coord{row: 1, col: 1}] = true
	}

	if string(inputString[10]) == "#" {
		p10[coord{row: 0, col: 0}] = true
	}
	if string(inputString[11]) == "#" {
		p10[coord{row: 0, col: 1}] = true
	}
	if string(inputString[15]) == "#" {
		p10[coord{row: 1, col: 0}] = true
	}
	if string(inputString[16]) == "#" {
		p10[coord{row: 1, col: 1}] = true
	}

	if string(inputString[12]) == "#" {
		p11[coord{row: 0, col: 0}] = true
	}
	if string(inputString[13]) == "#" {
		p11[coord{row: 0, col: 1}] = true
	}
	if string(inputString[17]) == "#" {
		p11[coord{row: 1, col: 0}] = true
	}
	if string(inputString[18]) == "#" {
		p11[coord{row: 1, col: 1}] = true
	}

	o00 = square{
		size:  2,
		posOn: &p00,
	}
	o01 = square{
		size:  2,
		posOn: &p01,
	}
	o10 = square{
		size:  2,
		posOn: &p10,
	}
	o11 = square{
		size:  2,
		posOn: &p11,
	}

	return

}

func (b board) String() string {
	ret := ""
	//	ret += fmt.Sprintf("Size: %d, divisor: %d", b.size, b.divisor)

	for i := 0; i <= (b.numSquaresOnSide * (b.squareSize + 1)); i++ {
		ret += "-"
	}
	ret += "\n"

	for squareRow := 0; squareRow < b.numSquaresOnSide; squareRow++ {
		for internalRow := 0; internalRow < b.squareSize; internalRow++ {
			ret += "|"
			for squareCol := 0; squareCol < b.numSquaresOnSide; squareCol++ {
				square := (*b.squares)[coord{row: squareRow, col: squareCol}]
				for internalCol := 0; internalCol < b.squareSize; internalCol++ {
					//					fmt.Printf("SquareSize; %d,   NumSquaresOnSide: %d, Row: %d, InternalRow: %d, squareRow: %d, Col: %d, InternalCol: %d, SquareCol: %d\n", b.squareSize, b.numSquaresOnSide, row, internalRow, squareRow, col, internalCol, squareCol)
					if square != nil {
						ret += square.getCharAt(coord{row: internalRow, col: internalCol})
					} else {
						ret += "X"
					}
				}
				ret += "|"
			}
			ret += "\n"
		}
		for i := 0; i <= (b.numSquaresOnSide * (b.squareSize + 1)); i++ {
			ret += "-"
		}
		ret += "\n"
	}

	return ret
}

func (s square) String() string {
	ret := ""
	for r := 0; r < s.size; r++ {
		for c := 0; c < s.size; c++ {
			ret += s.getCharAt(coord{row: r, col: c})
		}
		ret += "\n"
	}
	return ret
}

func (r rule) String() string {
	ret := "\n"
	if r.appliesToSquareSize == 2 {
		ret += fmt.Sprintf("        %s%s%s\n",
			r.output00.getCharAt(coord{row: 0, col: 0}),
			r.output00.getCharAt(coord{row: 0, col: 1}),
			r.output00.getCharAt(coord{row: 0, col: 2}),
		)

		ret += fmt.Sprintf("%s%s  =>  %s%s%s\n",
			r.inputSquare.getCharAt(coord{row: 0, col: 0}),
			r.inputSquare.getCharAt(coord{row: 0, col: 1}),
			r.output00.getCharAt(coord{row: 1, col: 0}),
			r.output00.getCharAt(coord{row: 1, col: 1}),
			r.output00.getCharAt(coord{row: 1, col: 2}),
		)
		ret += fmt.Sprintf("%s%s      %s%s%s\n",
			r.inputSquare.getCharAt(coord{row: 1, col: 0}),
			r.inputSquare.getCharAt(coord{row: 1, col: 1}),
			r.output00.getCharAt(coord{row: 2, col: 0}),
			r.output00.getCharAt(coord{row: 2, col: 1}),
			r.output00.getCharAt(coord{row: 2, col: 2}),
		)

	} else {
		ret += fmt.Sprintf("       -------\n")
		ret += fmt.Sprintf("       |%s%s|%s%s|\n",
			r.output00.getCharAt(coord{row: 0, col: 0}),
			r.output00.getCharAt(coord{row: 0, col: 1}),
			r.output01.getCharAt(coord{row: 0, col: 0}),
			r.output01.getCharAt(coord{row: 0, col: 1}),
		)
		ret += fmt.Sprintf("%s%s%s => |%s%s|%s%s|\n",
			r.inputSquare.getCharAt(coord{row: 0, col: 0}),
			r.inputSquare.getCharAt(coord{row: 0, col: 1}),
			r.inputSquare.getCharAt(coord{row: 0, col: 2}),
			r.output00.getCharAt(coord{row: 1, col: 0}),
			r.output00.getCharAt(coord{row: 1, col: 1}),
			r.output01.getCharAt(coord{row: 1, col: 0}),
			r.output01.getCharAt(coord{row: 1, col: 1}),
		)
		ret += fmt.Sprintf("%s%s%s    -------\n",
			r.inputSquare.getCharAt(coord{row: 1, col: 0}),
			r.inputSquare.getCharAt(coord{row: 1, col: 1}),
			r.inputSquare.getCharAt(coord{row: 1, col: 2}),
		)
		ret += fmt.Sprintf("%s%s%s    |%s%s|%s%s|\n",
			r.inputSquare.getCharAt(coord{row: 2, col: 0}),
			r.inputSquare.getCharAt(coord{row: 2, col: 1}),
			r.inputSquare.getCharAt(coord{row: 2, col: 2}),
			r.output10.getCharAt(coord{row: 0, col: 0}),
			r.output10.getCharAt(coord{row: 0, col: 1}),
			r.output11.getCharAt(coord{row: 0, col: 0}),
			r.output11.getCharAt(coord{row: 0, col: 1}),
		)
		ret += fmt.Sprintf("       |%s%s|%s%s|\n",
			r.output10.getCharAt(coord{row: 1, col: 0}),
			r.output10.getCharAt(coord{row: 1, col: 1}),
			r.output11.getCharAt(coord{row: 1, col: 0}),
			r.output11.getCharAt(coord{row: 1, col: 1}),
		)
		ret += fmt.Sprintf("       -------\n")

	}
	return ret
}

func (s square) getCharAt(c coord) string {
	_, isActive := (*s.posOn)[c]
	if isActive {
		return "#"
	} else {
		return "."
	}
}
