package d10

import (
	"fmt"
)

//var listSize = 5
var listSize = 256
//var input = []int{3, 4, 1, 5}
//var input = []int{46,41,212,83,1,255,157,65,139,52,39,254,2,86,0,204}
//var input = "1,2,3"
var input = "46,41,212,83,1,255,157,65,139,52,39,254,2,86,0,204"
//var input = "1,2,3"
var addon = []byte {17, 31, 73, 47, 23}
var skipSize = 0
var data []byte
var lengths []byte
var currentPosition = byte(0)
var denseHash [16]byte

func D10() {
	fillData()
	fixLengths()
	for i := 0; i < 64; i++ {
		transmogrify()
	}

	reduce()
//	fmt.Printf("Final result: %d\n", data[0] * data[1])
}

func reduce() {
	for denseIdx := 0; denseIdx < 16; denseIdx++ {
		sparseChar := byte(0)
		for i := 0; i < 16; i++ {
			sparseIdx := denseIdx * 16 + i
			sparseChar = sparseChar ^ data[sparseIdx]
		}
		denseHash[denseIdx] = sparseChar
	}

	fmt.Printf("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n", denseHash[0], denseHash[1], denseHash[2], denseHash[3], denseHash[4], denseHash[5], denseHash[6], denseHash[7], denseHash[8], denseHash[9], denseHash[10], denseHash[11], denseHash[12], denseHash[13], denseHash[14], denseHash[15])
}

func transmogrify() {
	fmt.Printf("Transmogrify start.  CurrentPosition %d,  SkipSize: %d\n", currentPosition, skipSize)
	for i := 0; i < len(lengths); i++ {
		length := lengths[i]
		fmt.Printf("Transmogrify round %d.  Length: %d\n", i + 1, length)

		var sublist []byte
		for j := byte(0); j < length ; j++ {
			sublist = append(sublist, data[currentPosition + j])
		}
		for j := byte(0); j < byte(len(sublist)); j++ {
			dataIdx := currentPosition + length - j - byte(1)
			data[dataIdx] = sublist[j]
		}

		fmt.Println(data)

		currentPosition = byte(int(currentPosition) + int(length) + skipSize)
		skipSize++
	}
}

func fixLengths() {
	lengths = []byte(input)
	for _, b := range addon {
		lengths = append(lengths, b)
	}
	fmt.Println(lengths)
}

func fillData() {
	for i := 0; i < listSize; i++ {
		data = append(data, byte(i))
	}
}