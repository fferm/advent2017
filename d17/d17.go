package d17

import (
	"fmt"
)

//var input = 3
var input = 304

//var maxNumber = 9
//var maxNumber = 2017
var maxNumber = 50 * 1000 * 1000

var searchFor = 0
//var searchFor = 2017

type node struct {
	data int
	nxt *node
}

var current *node

func D17() {
	// Setup
	current = &node{0, nil}
	current.nxt = current

	// Fill
	for i := 1; i <= maxNumber; i++ {
		if i % 100000 == 0 {
			fmt.Printf("Inserting number %d\n", i)
		}
		current = insert(i)

		for j := 0; j < input; j++ {
			current = current.nxt
		}
	}

	// Find right place
	for i := 0; i <= maxNumber; i++ {
		if current.data == searchFor {
			fmt.Println("Found right place")
			break
		}
		current = current.nxt
	}

	printStartingOn(current, 10)

	fmt.Printf("Answer is %d\n", current.nxt.data)
}

func insert(value int) *node {
	n := &node{value, current.nxt}
	current.nxt = n

	current = current.nxt

	return current
}

func printStartingOn(input *node, sizeToPrint int) {

	p := input
	for i := 0; i < sizeToPrint; i++ {
		fmt.Printf("%d ", p.data)
		p = p.nxt
	}
	fmt.Println()
}



/*	currentPosition := 1
	for i := 1; i <= maxNumber; i++ {
		insertIntoData(i, currentPosition)

		currentPosition++
		currentPosition = (currentPosition + input) % len(data)

		fmt.Println(data[1])
	}

	var posSearch int
	for i := 0; i < len(data); i++ {
		if data[i] == searchFor {
			posSearch = i
			break
		}
	}
	for i := posSearch - 5; i < posSearch + 5; i++ {
		fmt.Printf("Pos %d, value: %d\n", i, data[i])
	}*/

/*func insertIntoData(number, position int) {
	data = append(data[:position], append([]int{number}, data[position:]...)...)
}*/