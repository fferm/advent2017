package d8

import (
	"fmt"
	"strings"
	"io/ioutil"
	"strconv"
)

type instruction struct {
	register string
	delta int
	conditionRegister string
	conditionOperator string
	conditionLimit int
}

var instructions []instruction
var registers map[string]int = make(map[string]int)
var largestEverValue int = -99999999

func D8() {

	getInput()
	runInstructions()
	printRegisters()
}

func runInstructions() {
	for _, instruction := range instructions {
		if isConditionValid(instruction) {
			registerValue, registerExists := registers[instruction.register]

			var newValue int
			if registerExists {
				newValue = registerValue + instruction.delta
			} else {
				newValue = instruction.delta
			}

			if newValue > largestEverValue {
				largestEverValue = newValue
			}
			registers[instruction.register] = newValue
		}
	}
}

func isConditionValid(instruction instruction) bool {
	oldConditionValue, ok := registers[instruction.conditionRegister]
	if !ok {
		oldConditionValue = 0
	}

	if instruction.conditionOperator == "<" {
		return oldConditionValue < instruction.conditionLimit
	} else if instruction.conditionOperator == ">" {
		return oldConditionValue > instruction.conditionLimit
	} else if instruction.conditionOperator == ">=" {
		return oldConditionValue >= instruction.conditionLimit
	} else if instruction.conditionOperator == "==" {
		return oldConditionValue == instruction.conditionLimit
	} else if instruction.conditionOperator == "<=" {
		return oldConditionValue <= instruction.conditionLimit
	} else if instruction.conditionOperator == "!=" {
		return oldConditionValue != instruction.conditionLimit
	} else {
		fmt.Println("unknown operator: " + instruction.conditionOperator)
	}

	return false
}

func printRegisters() {
	fmt.Println(registers)

	largestValue := -9999999

	for _, v := range registers {
		if v > largestValue {
			largestValue = v
		}
	}

	fmt.Printf("Största värde nu: %d\n", largestValue)
	fmt.Printf("Största värde någonsin: %d\n", largestEverValue)
}

func getInput() {
	fullFile, _ := ioutil.ReadFile("d8/d8_input")

	rowsInFile := strings.Split(string(fullFile), "\r\n")

	for _, s := range rowsInFile {
		instruction := parseInputLine(s)
		instructions = append(instructions, instruction)
	}
}

func parseInputLine(inputLine string) instruction {
	words := strings.Split(inputLine, " ")

	register := words[0]
	
	delta, _ := strconv.Atoi(words[2])
	if words[1] == "dec" {
		delta *= -1
	}

	conditionRegister := words[4]

	conditionOperator := words[5]

	conditionLimit, _ := strconv.Atoi(words[6])

	instruction := instruction{
		register: register,
		delta: delta,
		conditionRegister: conditionRegister,
		conditionOperator: conditionOperator,
		conditionLimit: conditionLimit}

/*	fmt.Println(inputLine)
	fmt.Printf("Register: %s,     Delta: %d,    ConditionRegister: %s,    conditionOperator: %s,    ConditionLimit: %d\n", instruction.register, instruction.delta, instruction.conditionRegister, instruction.conditionOperator, instruction.conditionLimit)
	fmt.Println()*/

	return instruction
}

