package d5

import (
	"fmt"
	"io/ioutil"
	"strings"
	"strconv"
)


var input []int
func D5() {
	input = getInput5()

	idx := 0;
	counter := 0

	for idx < len(input) {
		offset := input[idx];
		if (offset >= 3) {
			input[idx] = input[idx] - 1
		} else {
			input[idx] = input[idx] + 1
		}
		idx = idx + offset
		counter++
	}

	fmt.Printf("%d steps \n", counter)
}

func getInput5() []int {
	fullFile, _ := ioutil.ReadFile("d5/d5_input")

	stringSplit := strings.Split(string(fullFile), "\n")

	var ret []int
	for _, s := range stringSplit {
		int, _ := strconv.Atoi(strings.Trim(s, "\r"))
		ret = append(ret, int)
	}
	return ret

}
