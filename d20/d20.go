package d20

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

type vector struct {
	x, y, z int
}

func (v vector) String() string {
	return fmt.Sprintf("<%d,%d,%d>", v.x, v.y, v.z)
}

type particle struct {
	number                           int
	position, velocity, acceleration *vector
	distance                         int
}

func (p particle) String() string {
	return fmt.Sprintf("Number: %d\tDistance: %d\tPosition: %s\tVelocity: %s\tAcceleration: %s", p.number, p.distance, *p.position, *p.velocity, *p.acceleration)
}

var numIterations = 2000

//var numIterations = 10

var filename = "d20/d20_input"

//var filename = "d20/d20_input_small"

func D20() {
	lines := getInput()

	fmt.Printf("%d particles in input\n", len(lines))
	fmt.Printf("%d iterations\n", numIterations)

	particles := makeParticlesFromInput(lines)

	for timeCounter := 0; timeCounter < numIterations; timeCounter++ {
		if timeCounter%1000 == 0 {
			fmt.Printf("Time: %d\n", timeCounter)
		}
		particles = analyzeCollitions(particles)

		for _, particle := range particles {
			updateParticle(particle)
		}
	}

	var particleWithLeastDistance *particle

	for _, particle := range particles {
		if particleWithLeastDistance == nil || particle.distance < particleWithLeastDistance.distance {
			particleWithLeastDistance = particle
		}
	}
	fmt.Printf("Least distance particle: %s\n", particleWithLeastDistance)

	fmt.Printf("Number of particles left: %d\n", len(particles))
}

func analyzeCollitions(particles map[int]*particle) map[int]*particle {
	particleIdsToRemove := make(map[int]interface{})

	for _, p1 := range particles {
		for _, p2 := range particles {
			if p2.number <= p1.number {
				continue
			}

			if p1.position.x == p2.position.x && p1.position.y == p2.position.y && p1.position.z == p2.position.z {
				particleIdsToRemove[p1.number] = true // Value does not matter, just wanted a map to search for numbers
				particleIdsToRemove[p2.number] = true
				fmt.Println(particleIdsToRemove)
			}
		}
	}

	for number, _ := range particleIdsToRemove {
		delete(particles, number)
	}

	return particles

}

func updateParticle(p *particle) {
	p.velocity.x += p.acceleration.x
	p.velocity.y += p.acceleration.y
	p.velocity.z += p.acceleration.z

	p.position.x += p.velocity.x
	p.position.y += p.velocity.y
	p.position.z += p.velocity.z

	p.distance = int(math.Abs(float64(p.position.x)) + math.Abs(float64(p.position.y)) + math.Abs(float64(p.position.z)))

	//	fmt.Println(p)
}

func makeParticlesFromInput(lines []string) map[int]*particle {
	ret := make(map[int]*particle)
	for i, line := range lines {
		firstParts := strings.Split(line, "v=")

		positionString := strings.Replace(firstParts[0], "p=", "", -1)

		secondParts := strings.Split(firstParts[1], "a=")

		velocityString := strings.Replace(secondParts[0], "v=", "", -1)
		accelerationString := strings.Replace(secondParts[1], "a=", "", -1)

		p := particle{
			number:       i,
			position:     getVectorFromString(positionString),
			velocity:     getVectorFromString(velocityString),
			acceleration: getVectorFromString(accelerationString),
		}

		ret[i] = &p
	}

	return ret
}

func getVectorFromString(input string) *vector {
	input = strings.Replace(input, "<", "", -1)
	input = strings.Replace(input, ">", "", -1)
	input = strings.Trim(input, " ")

	numbers := strings.Split(input, ",")

	x, _ := strconv.Atoi(numbers[0])
	y, _ := strconv.Atoi(numbers[1])
	z, _ := strconv.Atoi(numbers[2])

	v := vector{x, y, z}
	//	fmt.Println(v)
	return &v
}

func getInput() []string {
	ret := []string{}
	fullFile, _ := ioutil.ReadFile(filename)
	for _, s := range strings.Split(string(fullFile), "\n") {
		trimmed := strings.Trim(s, "\r")
		ret = append(ret, trimmed)
	}
	return ret
}
