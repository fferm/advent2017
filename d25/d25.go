package d25

import (
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
)

type rule struct {
	triggerState string
	triggerData  int
	writeData    int
	moveOffset   int
	nextState    string
}

var data map[int]int = make(map[int]int)
var maxPosition = 0
var minPosition = 0
var currentPosition = 0

//var filename = "d25/d25_input_small"

var filename = "d25/d25_input"

func D25() {
	startState, numSteps, rules := getInput()

	fmt.Printf("StartState: %s, numSteps: %d\n", startState, numSteps)
	fmt.Println(rules)

	currentState := startState
	for currentIteration := 1; currentIteration <= numSteps; currentIteration++ {

		//		printSituation(currentState)

		currentData, hasCurrentData := data[currentPosition]
		if !hasCurrentData {
			currentData = 0
		}

		applicableRule := findApplicableRule(rules, currentData, currentState)

		data[currentPosition] = applicableRule.writeData

		currentPosition += applicableRule.moveOffset
		if currentPosition < minPosition {
			minPosition = currentPosition
		}
		if currentPosition > maxPosition {
			maxPosition = currentPosition
		}

		currentState = applicableRule.nextState
	}

	checksum := 0
	for _, d := range data {
		if d == 1 {
			checksum++
		}
	}
	fmt.Printf("Checksum: %d\n", checksum)
}

func printSituation(currentState string) {
	fmt.Printf("CurrentState: %s   ", currentState)
	for i := minPosition; i <= maxPosition; i++ {
		var fmtString string
		if i == currentPosition {
			fmtString = "[%d] "
		} else {
			fmtString = " %d  "
		}

		data, hasData := data[i]
		if !hasData {
			data = 0
		}
		fmt.Printf(fmtString, data)
	}
	fmt.Printf("\n")
}

func findApplicableRule(rules []rule, currentData int, currentState string) rule {
	for _, r := range rules {
		if r.triggerState == currentState && r.triggerData == currentData {
			return r
		}
	}

	fmt.Printf("No rule found for state %s and data %d\n", currentState, currentData)
	return rules[0]
}

func getInput() (startState string, numSteps int, rules []rule) {
	fullFile, _ := ioutil.ReadFile(filename)

	sections := strings.Split(string(fullFile), "In state")

	firstLineSections := strings.Split(sections[0], "in state")
	startState = string(firstLineSections[1][1])

	secondLineSections := strings.Split(sections[0], "checksum after")
	num, _ := strconv.Atoi(strings.Split(secondLineSections[1][1:], " ")[0])

	allRules := []rule{}
	for i := 1; i < len(sections); i++ {
		currentSection := sections[i]
		rules := getRulesFromSection(currentSection)
		allRules = append(allRules, rules...)
	}

	return startState, num, allRules
}

func getRulesFromSection(input string) []rule {
	lines := strings.Split(input, "\n")

	stateRegexp, _ := regexp.Compile("[A-Z]")
	triggerState := stateRegexp.FindString(lines[0])

	valueRegexp, _ := regexp.Compile("[0-9]")
	triggerValue1, _ := strconv.Atoi(valueRegexp.FindString(lines[1]))
	triggerValue2, _ := strconv.Atoi(valueRegexp.FindString(lines[5]))

	writeValue1, _ := strconv.Atoi(valueRegexp.FindString(lines[2]))
	writeValue2, _ := strconv.Atoi(valueRegexp.FindString(lines[6]))

	var dir1, dir2 int

	if strings.Contains(lines[3], "right") {
		dir1 = 1
	} else {
		dir1 = -1
	}

	if strings.Contains(lines[7], "right") {
		dir2 = 1
	} else {
		dir2 = -1
	}

	nextState1 := stateRegexp.FindString(lines[4][10:])
	nextState2 := stateRegexp.FindString(lines[8][10:])

	rule1 := rule{
		triggerState: triggerState,
		triggerData:  triggerValue1,
		writeData:    writeValue1,
		moveOffset:   dir1,
		nextState:    nextState1,
	}
	rule2 := rule{
		triggerState: triggerState,
		triggerData:  triggerValue2,
		writeData:    writeValue2,
		moveOffset:   dir2,
		nextState:    nextState2,
	}

	ret := []rule{rule1, rule2}

	return ret
}
