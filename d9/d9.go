package d9

import (
    "fmt"
    "io/ioutil"
    "strings"
)

type row struct {
    text string
    score int
    garbageCharCount int
}

var data []*row
var fileContents string

var filename = "d9/d9_input"

func D9() {
    getInput()
    parseData()

    totalScore := 0
    totalGarbageCharCount := 0
    for _, row := range data {
        fmt.Printf("%d: %s\n", (*row).score, (*row).text)
        totalScore += (*row).score
        totalGarbageCharCount += (*row).garbageCharCount
    }
    fmt.Printf("Total score: %d\n", totalScore)
    fmt.Printf("Garbage char count: %d\n", totalGarbageCharCount)
}

func parseData() {
    score := 0
    groupLevel := 0
    inGarbage := false
    ignoreRune := false
    garbageCharCount := 0

    currentText := ""

    for _, c := range fileContents {
        rune := string(c)
        fmt.Println(rune)
        currentText += rune

        if ignoreRune {
            fmt.Println("Ignoring")
            ignoreRune = false
            continue
        }

        if rune == ">" {
            fmt.Println("Stop garbage")
            inGarbage = false
        }

        if inGarbage {
            fmt.Println("in garbage")
            if rune == "!" {
                fmt.Println("start ignore")
                ignoreRune = true
            } else {
                garbageCharCount++
            }
            continue
        }

        if rune == "{" {
            groupLevel++
            fmt.Printf("start group, level %d\n", groupLevel)
            if groupLevel == 1 {
                fmt.Println("start top-level")
                currentText = "" + rune
            }
        } else if rune == "}" {
            score += groupLevel
            fmt.Printf("end group, level %d    score: %d\n", groupLevel, score)
            groupLevel--
            if groupLevel == 0 {
                fmt.Println("end top-level")
                row := row{currentText, score, garbageCharCount}
                data = append(data, &row)
                score = 0
            }
        } else if rune == "<" {
            fmt.Println("start garbage")
            inGarbage = true
        } 
    }
}

func getInput() {
    fileBytes, _ := ioutil.ReadFile(filename)

    fileContents = string(fileBytes)
    fileContents = strings.Trim(fileContents, "\n")
    fileContents = strings.Trim(fileContents, "\r")
}
