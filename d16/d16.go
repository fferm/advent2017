package d16 

import(
    "fmt"
    "io/ioutil"
    "strings"
    "strconv"
)

//var originalData ="abcde"
var originalData = "abcdefghijklmnop"

type move struct {
    from int
    to int
}

var filename="d16/input"
//var filename="d16/input_small"

//var numDances = 1000000002
var numDances = 1000 * 1000 * 1000
func D16() {
    commands := getInput()

    data := originalData

    var stepsToRepeat int
    for stepsToRepeat = 1; true; stepsToRepeat++ {
        data = dance(commands, data)

        if data == originalData {
            fmt.Printf("%d\n", stepsToRepeat)
            break
        }
    }

    fmt.Printf("Data after check: %s\n", data)

    for i := 0; i < (numDances % stepsToRepeat) ; i++ {
        data = dance(commands, data)
    }

    fmt.Printf("When ready: %s\n", data)
}

func dance(commands []string, data string) string {
    for _, command := range commands {

        if string(command[0]) == "s" {
            william, _ := strconv.Atoi(string(command[1:]))

            pos := len(data) - william

            data = string(data[pos:]) + string(data[0:pos])

        } else if string(command[0]) == "x" {
            numberStrings := strings.Split(string(command[1:]), "/")

            n1, _ := strconv.Atoi(numberStrings[0])
            n2, _ := strconv.Atoi(numberStrings[1])

            data = swapTwo(data, n1, n2)
        } else if string(command[0]) == "p" {
            numberStrings := strings.Split(string(command[1:]), "/")

            n1 := strings.Index(data, numberStrings[0])            
            n2 := strings.Index(data, numberStrings[1])            

            data = swapTwo(data, n1, n2)
        }
    }

    return data
}

func swapTwo(data string, i1, i2 int) string {
    if i1 > i2 {
        s := i2
        i2 = i1
        i1 = s
    }
    data = string(data[0:i1]) + string(data[i2]) + string(data[i1 + 1:i2]) + string(data[i1]) + string(data[i2 + 1:])

    return data

}

func getInput() []string {
    fullFile, _ := ioutil.ReadFile(filename)
    return strings.Split(string(fullFile), ",")
}
