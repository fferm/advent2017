public class d15 {
    public static void main(String[] args) {
        d15 d15 = new d15();
        d15.run();
    }

    long aStart = 634;
    long bStart = 301;

    long aFactor = 16807;
    long bFactor = 48271;

    long divisor =2147483647;

    int numValues = 5000000;

    public void run() {
        long a = aStart;
        long b = bStart;

        int numMatches = 0;

        for (int i = 0; i < numValues; i++) {
            //if (i % 10 == 0) System.out.println("i = " + i);
            a = generate(a, aFactor, divisor, 4);
            b = generate(b, bFactor, divisor, 8);

            if (compare(a, b)) {    
                numMatches++;
    //          fmt.Printf("Match on value %d\n", i+1)
            }
        }

        System.out.println("Number of matches: " + numMatches);
    }

    private long generate(long prevValue, long factor, long divisor, int multiple) {
        long candidate;
        while(true) {
            candidate = (prevValue * factor) % divisor;
            //System.out.println("Candidate: " + candidate + "    rest: " + (candidate % multiple));
            if ((candidate % multiple) == 0) {
                return candidate;
            }
            prevValue = candidate;
        }
    }

    private boolean compare(long a, long b) {
        return (a & 0xffff) == (b & 0xffff);
    }

}



