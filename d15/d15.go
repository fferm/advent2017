package d15

import (	
	"fmt"
)
// Example
/*var aStart = 65
var bStart = 8921*/

// My values
var aStart = 634
var bStart = 301

var aFactor = 16807
var bFactor = 48271

var divisor = 2147483647

//var numValues = 40000000
var numValues = 5000000

func D15() {
	var a int
	var b int

	a = aStart
	b = bStart

	numMatches := 0
	for i := 0; i < numValues; i++ {
		a = generate(a, aFactor, divisor, 4)
		b = generate(b, bFactor, divisor, 8)

//		fmt.Printf("%d\t%d\n", a, b)
		if compare(a, b) {
			numMatches++
//			fmt.Printf("Match on value %d\n", i+1)
		}
	}
	fmt.Printf("Number of matches: %d\n", numMatches)
}

func generate(prevValue int, factor int, divisor int, multiple int) int {
	var candidate int
	for {
		candidate = (prevValue * factor) % divisor
		if candidate % multiple == 0 {
			return candidate
		}
		prevValue = candidate
	}
}

func compare(a, b int) bool {
	return (a & 0xffff) == (b & 0xffff)
}