package d7

import (
	"fmt"
	"strings"
	"io/ioutil"
	"strconv"
)

type program struct {
	name string
	weight int
	depNames []string
	parentName string
	towerWeight int
}

var programs map[string]program
var root program

func D7() {
	programs = make(map[string]program)
	getInput()
	setParent()

	for _, v := range programs {
		if len(v.parentName) == 0 {
			root = v
			break;
		}
	}
	fmt.Printf("root: %s\n", root.name)

	setTowerWeight(root.name)

	findImbalanceRoot(root.name)
}

func findImbalanceRoot(currentProgramName string) {
	currentProgram := programs[currentProgramName]

	if len(currentProgram.depNames) == 0 {
		return
	} else {
		subProgramWeightsByName := make(map[string]int)
		
		for _, subProgramName := range currentProgram.depNames {
			subProgram := programs[subProgramName]
			subProgramTowerWeight := subProgram.towerWeight
			subProgramWeightsByName[subProgramName] = subProgramTowerWeight
		}

		if allEqual(subProgramWeightsByName) {
			for subProgramName, _ := range subProgramWeightsByName {
				findImbalanceRoot(subProgramName)
			}
		} else {
			fmt.Println(currentProgramName)
			fmt.Println(subProgramWeightsByName)
			fmt.Println()
			for subProgramName, _ := range subProgramWeightsByName {
				findImbalanceRoot(subProgramName)
			}
		}
	}
}

func allEqual(input map[string]int) bool {
	prev := -1
	for _, i := range input {
		if prev != -1 && prev != i {
			return false
		}
		prev = i
	}
	return true
}

func setTowerWeight(programName string) {
	towerWeight := programs[programName].weight

	if len(programs[programName].depNames) > 0 {
		for _, depName := range programs[programName].depNames {

			setTowerWeight(depName)

			towerWeight +=  programs[depName].towerWeight
		}

	}

	currentProgram := programs[programName]
	currentProgram.towerWeight = towerWeight
	programs[programName] = currentProgram
}

func setParent() {
	for _, mainProgram := range programs {
		if len(mainProgram.depNames) > 0 {
			for _, dependentName := range mainProgram.depNames {
				dependentProgram, _ := programs[dependentName]

				dependentProgram.parentName = mainProgram.name
				programs[dependentName] = dependentProgram
			}
		}
	}
}

func getInput() {
	fullFile, _ := ioutil.ReadFile("d7/d7_input")

	stringSplit := strings.Split(string(fullFile), "\n")

	for _, s := range stringSplit {
		p := parseInputLine(s)
		programs[p.name] = p
	}

}

func parseInputLine(inputLine string) program {
	words := strings.Split(inputLine, " ")

	name := words[0]

	numString := strings.Trim(strings.Trim(strings.Trim(words[1], "\r"), ")"), "(")
	weight, _ := strconv.Atoi(numString)

	var dependents []string

	if len(words) > 3 {
		dependents = words[3:]
		for i, _ := range dependents {
			dependents[i] = strings.Trim(strings.Trim(dependents[i], ","), "\r")
		}
	} else {
		dependents = nil
	}

	p := program{name, weight, dependents, "", -1}

	return p
}
