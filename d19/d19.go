package d19

import (
    "fmt"
    "io/ioutil"
    "strings"
)

var filename="d19/d19_input"
//var filename="d19/d19_input_small"

var DOWN coordinate = coordinate{x: 0, y: 1}
var LEFT coordinate = coordinate{x: -1, y: 0}
var UP coordinate = coordinate{x: 0, y: -1}
var RIGHT coordinate = coordinate{x: 1, y: 0}

type coordinate struct {
    x, y int
}

var path []string
var letters = ""
var numberOfSteps = 0
func D19() {
    path = getInput()

    start := findStartingPosition()

    cont := true
    pos := start
    direction := DOWN

    for cont {
        pos, direction, cont = moveOneStep(pos, direction)
        numberOfSteps++
//        fmt.Printf("%d, %d, %v\n", pos, direction, cont)
    }

    fmt.Printf("Found letters: %s\n", letters)
    fmt.Printf("Stepped %d steps\n", numberOfSteps - 1)
}

func moveOneStep(pos, dir coordinate) (newPos, newDir coordinate, cont bool) {
    stringAtCurrentPosition := string(path[pos.y][pos.x])

    newDir = dir
    cont = false

    fmt.Printf("Found %s at %d\n", stringAtCurrentPosition, pos)

    switch stringAtCurrentPosition {
    case "|":
        cont = true
    case "-":
        cont = true
    case "+":
        if dir == DOWN || dir == UP {
            toLeft := string(path[pos.y][pos.x - 1])
            toRight := string(path[pos.y][pos.x + 1])
            if toLeft != " " {
                newDir = LEFT
                cont = true
            } else if toRight != " " {
                newDir = RIGHT
                cont = true
            } else {
                fmt.Println("Found + but nowhere to turn to at %d\n", pos)
            }
        } else {
            toUp := string(path[pos.y - 1][pos.x])
            toDown := string(path[pos.y + 1][pos.x])
            if toUp != " " {
                newDir = UP
                cont = true
            } else if toDown != " " {
                newDir = DOWN
                cont = true
            } else {
                fmt.Println("Found + but nowhere to turn to at %d\n", pos)
            }
        }
    case "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "Z":
        letters += stringAtCurrentPosition
        cont = true
    case " ":
        cont = false
        fmt.Printf("Stopping at %d\n", pos)
    default:
        fmt.Printf("Unknown char %s at position %d with direction %d\n", stringAtCurrentPosition, pos, dir)
    }

    newPos = coordinate{x: pos.x + newDir.x, y: pos.y + newDir.y}

    return
}

func findStartingPosition() coordinate {
    firstLine := path[1]

    for i, c := range firstLine {
        if string(c) == "|" {
            return coordinate{i, 1}
        }
    }
    return coordinate{-1, 1}
}


func getInput() []string {
    ret := []string{}
    fullFile, _ := ioutil.ReadFile(filename)
    for _, s := range strings.Split(string(fullFile), "\n") {
        trimmed := strings.Trim(s, "\r")
        padded := " " + trimmed + " "
        ret = append(ret, padded)
    }

    paddingTop := ""
    paddingBottom := ""
    for i:= 0; i < len(ret[0]); i++ {
        paddingTop += " "
        paddingBottom += " "
    }

    ret = append([]string{paddingTop}, ret...)
    ret = append(ret, paddingBottom)

    fmt.Printf("ret has %d lines\n", len(ret))
    return ret
}
