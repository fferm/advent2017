package d13

import (
    "fmt"
    "io/ioutil"
    "strings"
    "strconv"
)

var filename = "d13/d13_input"

type layer struct {
    depth int
    r int
    scannerPosition int
    scannerDirection int
}

var layers map[int]*layer = make(map[int]*layer)
var maxDepth int = 0
var maxRange int = 0

var myPosition int = -1
var totalScore int = 0

func D13() {
    getInput()

    for delay := 0; delay < 200000000000000000; delay++ {
        fmt.Printf("Simulation with delay: %d\n", delay)
        
        var caughtAtAll bool = false

        for _, currentLayerPtr := range layers {
            arriveTime := currentLayerPtr.arriveTimeWithDelay(delay)

            if currentLayerPtr.isLayerCatchingAtTime(arriveTime) {
//                fmt.Printf("Caught at layer %d\n", (*currentLayerPtr).depth)
                caughtAtAll = true
            }
        }

        if !caughtAtAll {
            fmt.Printf("A delay of %d does not catch\n", delay)
            break
        }
    }
    fmt.Println("Finished")
/*    for delay := 200000;  delay < 2000000; delay++ {
        caught := runSimulation(delay)
        if !caught {
            fmt.Printf("If delay is %d you will not get caught\n", delay)
            break
        }
    }
    fmt.Println("Finished")*/
}

func (layerPtr *layer) isLayerCatchingAtTime(time int) bool {
    divisor := 2 * ((*layerPtr).r - 1)
    return (time % divisor) == 0
}

func (layerPtr *layer) arriveTimeWithDelay(delay int) int {
    return (*layerPtr).depth + delay
}

/*func runSimulation(delay int) (didIGetCaught bool) {
    reset()

    fmt.Printf("Starting simulation with delay %d\n", delay)
    myPosition = 0 - delay - 1

    totalScore = 0

    var gotCaught bool
    for myPosition < maxDepth {
        gotCaught = move()
//        printSituation()

        if gotCaught {
            return true
        }
        tick()
    }

    return false

//    return totalScore != 0
}*/

/*func reset() {
    for _, lPtr := range layers {
        (*lPtr).scannerPosition = 0
        (*lPtr).scannerDirection = 1
    }
}*/

/*func move() (gotCaught bool) {
    myPosition++

    currentLayerPtr, currentLayerExists := layers[myPosition]
    
    if !currentLayerExists {
        return
    }

    if (*currentLayerPtr).scannerPosition == 0 {
        return true
//        scoreForThisLayer := (*currentLayerPtr).depth * (*currentLayerPtr).r
//        totalScore += scoreForThisLayer
    }
    return false
}*/

/*func tick() {
    for _, layerPtr := range layers {
        (*layerPtr).scannerPosition += (*layerPtr).scannerDirection

        if (*layerPtr).scannerPosition == ((*layerPtr).r - 1) || (*layerPtr).scannerPosition == 0 {
            (*layerPtr).scannerDirection *= -1
        }
    }

}*/

/*func printSituation() {
    fmt.Printf("Time: %d\n", myPosition)
    for i := 0; i <= maxDepth; i++ {
        fmt.Printf("%02d: ", i)

        currentLayerPtr, currentLayerExists := layers[i]
        crashMessage := ""

        for j := 0; j < maxRange; j++ {
            var currentStringFirst string
            var currentStringSecond string
            var currentStringThird string

            if j == 0 && i == myPosition {
                currentStringFirst = "("
                    if currentLayerExists && j == (*currentLayerPtr).scannerPosition {
                        currentStringSecond = "S"
                        crashMessage = fmt.Sprintf("--- Caught.  %d", totalScore)
                    } else {
                        currentStringSecond = "."
                    }
                currentStringThird = ")"
            } else if currentLayerExists {
                if j < (*currentLayerPtr).r {
                    currentStringFirst = "["
                    currentStringThird = "]"
                } else {
                    currentStringFirst = " "
                    currentStringThird = " "
                }

                if j == (*currentLayerPtr).scannerPosition {
                    currentStringSecond = "S"
                } else {
                    currentStringSecond = " "
                }
            } else {
                currentStringFirst = " "
                currentStringSecond = " "
                currentStringThird = " "
            }


            fmt.Printf("%s%s%s ", currentStringFirst, currentStringSecond, currentStringThird)
        }

        fmt.Printf("%s\n", crashMessage)
    }
    fmt.Println()
}*/

func getInput() {
    fullFile, _ := ioutil.ReadFile(filename)

    split := strings.Split(string(fullFile), "\n")

    for _, line := range split {
        line = strings.Trim(line, "\r")

        split := strings.Split(line, ":")

        depth, _ := strconv.Atoi(strings.Trim(split[0], " "))
        r, _ := strconv.Atoi(strings.Trim(split[1], " "))

        layer := layer{depth, r, 0, 1}

        if depth > maxDepth {
            maxDepth = depth
        }
        if r > maxRange {
            maxRange = r
        }

        fmt.Println(layer)
        layers[depth] = &layer
    }
}

