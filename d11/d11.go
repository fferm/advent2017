package d11

import (
	"fmt"
	"math"
	"strings"
	"io/ioutil"
)

// https://www.redblobgames.com/grids/hexagons/

type hexCoord struct {
	x int
	y int
	z int
}

func (c hexCoord) distance() int {
	return int((math.Abs(float64(c.x)) + math.Abs(float64(c.y)) + math.Abs(float64(c.z))) / 2)
}

func (c hexCoord) move(direction string) hexCoord {
	var ret hexCoord
	switch direction {
	case "n":
		ret = hexCoord{c.x, 		c.y + 1, 	c.z - 1}
	case "ne":
		ret = hexCoord{c.x + 1, 	c.y, 		c.z - 1}
	case "se":
		ret = hexCoord{c.x + 1, 	c.y - 1, 	c.z}
	case "s":
		ret = hexCoord{c.x, 		c.y - 1, 	c.z + 1}
	case "sw":
		ret = hexCoord{c.x - 1, 	c.y, 		c.z + 1}
	case "nw":
		ret = hexCoord{c.x - 1, 	c.y + 1, 	c.z}
	default:
		fmt.Println("UNKNOWN DIRECTION: " + direction)
	}

	return ret
}

//var inputString = "se,sw,se,sw,sw"
var inputString string

func D11() {
	h := hexCoord{0, 0, 0}
	largestDistance := 0
	inputBytes, _ := ioutil.ReadFile("d11/d11_input")

	inputString = string(inputBytes)

	moves := breakInputString(inputString)
	for _, m := range moves {
		h = h.move(m)

		distance := h.distance()
		if distance > largestDistance {
			largestDistance = distance
		}
		fmt.Printf("Moving %s to %d.  Distance is now: %d\n", m, h, distance)
	}

	fmt.Printf("LargestDistance was %d\n", largestDistance)
}

func breakInputString(inputString string) []string {
	inputString = strings.Trim(inputString, "\n")
	inputString = strings.Trim(inputString, "\r")

	return strings.Split(inputString, ",")
}

