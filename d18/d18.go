package d18

import (
	"fmt"
	"strings"
	"io/ioutil"
	"strconv"
)

var filename="d18/d18_input"
//var filename="d18/d18_input_small"

var instructions []string

type program struct{
	id int
	registers map[string]int
	queue []int
	currentInstruction int
	sendCounter int
}

func D18() {
	p0 := program{id: 0, registers: map[string]int{"p":0 }, queue: []int{}, currentInstruction: 0, sendCounter: 0}
	p1 := program{id: 1, registers: map[string]int{"p":1 }, queue: []int{}, currentInstruction: 0, sendCounter: 0}

	instructions = getInput()

	didRunP0 := true
	didRunP1 := true
	for didRunP0 || didRunP1 {
		didRunP0 = false
		didRunP1 = false
		for {
			canContinue := p0.runInstruction(&p1)
			if !canContinue {
				break
			}
			didRunP0 = true
		}
		for {
			canContinue := p1.runInstruction(&p0)
			if !canContinue {
				break
			}
			didRunP1 = true
		}
	}
	fmt.Println(p0)
	fmt.Println(p1)
	fmt.Printf("P1 sent %d messages\n", p1.sendCounter)
}

func (p *program) runInstruction(otherProgram *program) (camContinue bool) {
	if p.currentInstruction < 0 || p.currentInstruction >= len(instructions) {
		return false
	}

	instruction := instructions[p.currentInstruction]

	instructionSplit := strings.Split(instruction, " ")
	instructionType := instructionSplit[0]
	param1 := instructionSplit[1]

	var param2 string
	if len(instructionSplit) >= 3 {
		param2 = instructionSplit[2]
	} else {
		param2 = ""
	}

	fmt.Printf("Program: %d\t%s:\t", p.id, instruction)
	switch instructionType {
	case "set":
		p.set(param1, param2)
	case "add":
		p.add(param1, param2)
	case "mul":
		p.mul(param1, param2)
	case "mod":
		p.mod(param1, param2)
	case "snd":
		p.snd(param1, otherProgram)
	case "rcv":
		couldReceive := p.receive(param1)
		if !couldReceive {
			return false
		}
	case "jgz":
		p.jump(param1, param2)
	default:
		fmt.Printf("Unknown instruction: %s\n", instruction)
	}

	p.currentInstruction++

	return p.currentInstruction >= 0 && p.currentInstruction < len(instructions)
}

func (p *program) snd(param string, otherProgram *program) {
	value := p.getAsNumber(param)
	otherProgram.queue = append(otherProgram.queue, value)

	p.sendCounter++

	fmt.Printf("Adding %d to other program queue\n", value)
}

func (p *program) set(register, param2 string) {
	value := p.getAsNumber(param2)

	p.registers[register] = value

	fmt.Printf("Sets register %s to %d\n", register, value)
}

func (p *program) add(param1, param2 string) {
	v1 := p.registers[param1]
	v2 := p.getAsNumber(param2)

	p.registers[param1] = v1 + v2

	fmt.Printf("Adds %d and %d into register %s\n", v1, v2, param1)
}

func (p *program) mul(param1, param2 string) {
	v1 := p.registers[param1]
	v2 := p.getAsNumber(param2)

	p.registers[param1] = v1 * v2

	fmt.Printf("Multiplies %d and %d into register %s\n", v1, v2, param1)
}

func (p *program) mod(param1, param2 string) {
	v1 := p.registers[param1]
	v2 := p.getAsNumber(param2)

	p.registers[param1] = v1 % v2

	fmt.Printf("Mods %d and %d with result %d into register %s\n", v1, v2, v1 % v2, param1)
}

func (p *program) receive(param1 string) (couldReceive bool) {
	if len(p.queue) == 0 {
		fmt.Println("Cannot receive since queue is empty")
		return false
	}
	value := p.queue[0]
	p.queue = p.queue[1:]

	p.registers[param1] = value
	fmt.Printf("Setting register %s to recovered value %d\n", param1, value)

	return true
}

func (p *program) jump(param1, param2 string) {
	// return one less than wanted, since the main loop has an increment of currentInstruction
	v1 := p.getAsNumber(param1)
	v2 := p.getAsNumber(param2)

	if v1 > 0 {
		p.currentInstruction += v2
		p.currentInstruction--
		fmt.Printf("Jump %d steps since parameter was %d\n", v2, v1)
	} else {
		fmt.Printf("Parameter is 0, do nothing \n")
	}
}

func (p *program) getAsNumber(param string) int {
	value, err := strconv.Atoi(param)

	if err != nil {
		value = p.registers[param]
	}
	return value
}

func getInput() []string {
	ret := []string{}
    fullFile, _ := ioutil.ReadFile(filename)
    for _, s := range strings.Split(string(fullFile), "\n") {
    	ret = append(ret, strings.Trim(s, "\r"))
    }
    return ret
}
