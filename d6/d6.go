package d6

import (
    "fmt"
)

var prevHashes map[int][]int

func D6() {
    //data := []int{0, 2, 7, 0}
    data := []int{5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6}
    prevHashes = make(map[int][]int)

    fmt.Println(data)

    counter := 0
    for !foundBefore(data) {
        prevHashes[hash(data)] = data

        redistribute(data)

        counter++
    }
    fmt.Printf("Ute efter %d cykler\n", counter)

    hashToLookFor := hash(data)
    fmt.Printf("Letar efter %v\n", data)

    newCounter := 1
    redistribute(data)

    for hashToLookFor != hash(data) {
        redistribute(data)
        newCounter++
    }

    fmt.Printf("%d cykler\n", newCounter)


}

func redistribute(data []int) {
    maxIdx, maxValue := max(data)

    data[maxIdx] = 0

    for i := 1; i <= maxValue; i++ {
        newIdx := (maxIdx + i) % len(data)
        data[newIdx] = data[newIdx] + 1
    }
    fmt.Println(data)
}

func hash(input []int) int {
    hash := 0

    for _, i := range input {
        hash *= 21
        hash += i
    }

    return hash
}

func foundBefore(input []int) bool {
    hashValue := hash(input)

    _, ok := prevHashes[hashValue]

    return ok
}

func max(input []int) (idx, value int) {
    idx = 0
    value = 0

    for i, v := range input {
        if v > value {
            value = v
            idx = i
        }
    }

    return
}
