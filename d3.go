package main

import (
    "fmt"
    )

type coord struct {
    x int
    y int
}

var mapping map[coord]int
var direction coord
 
func d3() {
    input := 347991
//    input := 40

    mapping = make(map[coord]int)

    UP := coord{0, 1}
    DOWN := coord{0, -1}
    LEFT := coord{-1, 0}
    RIGHT := coord{1, 0}

    var prevLocation coord
    var newLocation coord
    var value int

    for i := 1; i <= input; i++ {
        var numMappedInCross int
        if i==1 {
            prevLocation = coord{0,0}
            value = i
            mapping[prevLocation] = value
            direction = RIGHT
        } else {
            newX := prevLocation.x + direction.x
            newY := prevLocation.y + direction.y

            newLocation = coord{newX, newY}

            value = getValue(newLocation)

            if value > input {
                fmt.Printf("Final result %d \n", value)
                break
            }
            mapping[newLocation] = value

            numMappedInCross = 0
            if isMapped(coord{newX + 1, newY}) {
                numMappedInCross += 1;
            } 
            if isMapped(coord{newX - 1, newY}) {
                numMappedInCross += 1;
            } 
            if numMappedInCross < 2 && isMapped(coord{newX, newY + 1}) {
                numMappedInCross += 1;
            } 
            if numMappedInCross < 2 && isMapped(coord{newX, newY - 1}) {
                numMappedInCross += 1;
            }

            if numMappedInCross == 1 {
                // Turn
                if direction == RIGHT {
                    direction = UP
                } else if direction == UP {
                    direction = LEFT
                } else if direction == LEFT {
                    direction = DOWN
                } else if direction == DOWN {
                    direction = RIGHT
                }
            }
            prevLocation = newLocation
        }

        fmt.Printf("i: %d   coord: %+v  value: %d direction %+v  numMappedInCross: %d \n", i, newLocation, value, direction, numMappedInCross)
    }
}

func isMapped(c coord) bool {
    if _    , ok := mapping[c]; ok {
        return true
    }
    return false
}

func getValue(c coord) int {
    e := mapping[coord{c.x + 1, c.y}]
    ne := mapping[coord{c.x + 1, c.y + 1}]
    n := mapping[coord{c.x, c.y + 1}]
    nv := mapping[coord{c.x - 1, c.y + 1}]
    v := mapping[coord{c.x - 1, c.y}]
    sv := mapping[coord{c.x - 1, c.y - 1}]
    s := mapping[coord{c.x, c.y - 1}]
    se := mapping[coord{c.x + 1, c.y - 1}]

    return e + ne + n + nv + v + sv + s + se
}