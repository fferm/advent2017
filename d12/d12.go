package d12

import (
	"fmt"
	"io/ioutil"
	"strings"
	"strconv"
)

var filename = "d12/d12_input"

type node struct {
	id int
	relId []int
	groupNum int
}

var nodes map[int]*node = make(map[int]*node)

func D12() {
	getInput()

	currentGroupNum := 0
	foundNewGroup := true
	for foundNewGroup {
		for _, nodePtr := range nodes {
			foundNewGroup = false

			if (*nodePtr).groupNum == -1 {
				currentGroupNum++
				foundNewGroup = true

				traverse(nodePtr, currentGroupNum)
				continue
			}
		}
	}
	for i, nodePtr := range nodes {
		fmt.Printf("i: %d, node: %v\n", i, *nodePtr)
	}

	fmt.Printf("%d groups\n", currentGroupNum)
}

func getInput() {
	fullFile, _ := ioutil.ReadFile(filename)

	split := strings.Split(string(fullFile), "\n")

	for _, line := range split {
		line = strings.Trim(line, "\r")
		parseLine(line)
	}
}

func parseLine(line string) {
	split := strings.Split(line, "<->")

	nodeId, _ := strconv.Atoi(strings.Trim(split[0], " "))

	var relatedIds []int
	for _, relatedStr := range strings.Split(split[1], ",") {
		intValue, _ := strconv.Atoi(strings.Trim(relatedStr, " "))
		relatedIds = append(relatedIds, intValue)
	}

	node := node{nodeId, relatedIds, -1}

	nodes[nodeId] = &node
}

func traverse(currentNode *node, groupNum int) {
	(*currentNode).groupNum = groupNum

	fmt.Printf("Visiting node %d with group %d\n", (*currentNode).id, groupNum)
	for _, relatedId := range (*currentNode).relId {
		relatedPtr := nodes[relatedId]
		if (*relatedPtr).groupNum != groupNum {
			traverse(relatedPtr, groupNum)
		}
	}
}

