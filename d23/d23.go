package d23

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

var filename = "d23/d23_input"

//var filename="d23/d23_input_small"

var instructions []string

var debug = false

type program struct {
	id                 int
	registers          map[string]int
	queue              []int
	currentInstruction int
	mulCounter         int
	instructionCounter int
}

func D23_analyzed() {
	//a: 0, b: 99, c: 99, d: 99, e: 99, f: 0, g: 0, h:3
	a := 0
	b := 0
	c := 0
	d := 0
	e := 0
	f := 0
	g := 0
	h := 0

	a = 1

	if a == 0 {
		b = 65
		c = 99
	} else {
		b = 109900
		c = 126900
	}

	for ; b <= c; b += 17 {
		fmt.Printf("b: %d\n", b)
		f = 1 //set f 1

		foundFactors := false
		for d := 2; (!foundFactors) && (d < b); d++ {

			for e := b; (!foundFactors) && (e > 2); e-- {

				if d*e == b { //jnz g 2
					foundFactors = true
					fmt.Printf("%d = %d * %d\n", b, d, e)
					f = 0 //set f 0
				}
			}
		}

		if f == 0 { //jnz f 2
			h += 1 //sub h -1
		}
	} //jnz 1 -23

	fmt.Printf("a: %d, b: %d, c: %d, d: %d, e: %d, f: %d, g: %d, h:%d\n", a, b, c, e, d, f, g, h)

}

func D23_analyzed_first() {

	//a: 0, b: 99, c: 99, d: 99, e: 99, f: 0, g: 0, h:1
	a := 0
	b := 0
	c := 0
	d := 0
	e := 0
	f := 0
	g := 0
	h := 1

	//	a = 1

	//jnz a 2
	//jnz 1 5
	if a != 0 {
		b = 109900
		c = 126900
	} else {
		b = 99 - 4*17
		c = 99
	}

	run := false
	for ; b < c; b += 17 {
		fmt.Println("Hej 1")
		d = 2
		for run /*g != 0*/ {
			fmt.Println("Hej 2")
			for e := 2; e != b; e++ {
				fmt.Println("Hej 3")
				if d*e != b {
					fmt.Println("Hej 4")
					h += 1
				}

			}

			d += 1

			run = d != b //	g = d - b
		}

		if b == c {
			break
		}

		run = true
	}

	fmt.Printf("a: %d, b: %d, c: %d, d: %d, e: %d, f: %d, g: %d, h:%d\n", a, b, c, e, d, f, g, h)
}

func D23() {
	p0 := program{id: 0, registers: map[string]int{}, queue: []int{}, currentInstruction: 0, mulCounter: 0, instructionCounter: 0}

	//p0.registers["a"] = 1
	/*	p0.registers["b"] = 99
		p0.registers["c"] = 99
		p0.registers["f"] = 0
		p0.registers["d"] = 99
		p0.registers["e"] = 99
		p0.registers["g"] = 0
		p0.registers["h"] = 0*/

	instructions = getInput()

	for /*p0.instructionCounter < 100*/ {
		if !debug && p0.instructionCounter%10000 == 0 {
			fmt.Printf("At instructionCounter %d   a: %d, b: %d, c: %d, d: %d, e: %d, f: %d, g: %d, h:%d\n", p0.instructionCounter, p0.registers["a"], p0.registers["b"], p0.registers["c"], p0.registers["e"], p0.registers["d"], p0.registers["f"], p0.registers["g"], p0.registers["h"])
		}
		canContinue := p0.runInstruction()
		if !canContinue {
			break
		}
	}
	fmt.Println(p0)
	fmt.Printf("Mul was called %d times\n", p0.mulCounter)
	fmt.Printf("register h has value: %d\n", p0.registers["h"])
	fmt.Printf("a: %d, b: %d, c: %d, d: %d, e: %d, f: %d, g: %d, h:%d\n", p0.registers["a"], p0.registers["b"], p0.registers["c"], p0.registers["e"], p0.registers["d"], p0.registers["f"], p0.registers["g"], p0.registers["h"])
}

func (p *program) runInstruction() (camContinue bool) {
	if p.currentInstruction < 0 || p.currentInstruction >= len(instructions) {
		return false
	}
	p.instructionCounter++

	instruction := instructions[p.currentInstruction]

	instructionSplit := strings.Split(instruction, " ")
	instructionType := instructionSplit[0]
	param1 := instructionSplit[1]

	var param2 string
	if len(instructionSplit) >= 3 {
		param2 = instructionSplit[2]
	} else {
		param2 = ""
	}

	if debug {
		fmt.Printf("Instruction: %d\tProgramCounter: %d\t%s:\t", p.instructionCounter, p.currentInstruction+1, instruction)
	}
	switch instructionType {
	case "set":
		p.set(param1, param2)
	case "sub":
		p.sub(param1, param2)
	case "mul":
		p.mul(param1, param2)
	case "jnz":
		p.jump(param1, param2)
	default:
		if debug {
			fmt.Printf("Unknown instruction: %s\n", instruction)
		}
	}

	p.currentInstruction++

	return p.currentInstruction >= 0 && p.currentInstruction < len(instructions)
}

func (p *program) set(register, param2 string) {
	value := p.getAsNumber(param2)

	p.registers[register] = value

	if debug {
		fmt.Printf("Sets register %s to %d\n", register, value)
	}
}

func (p *program) sub(register, param2 string) {
	value := p.getAsNumber(param2)

	p.registers[register] -= value

	if debug {
		fmt.Printf("Decreases register %s by %d to %d\n", register, value, p.registers[register])
	}
}

func (p *program) mul(param1, param2 string) {
	v1 := p.registers[param1]
	v2 := p.getAsNumber(param2)

	p.registers[param1] = v1 * v2
	p.mulCounter++

	if debug {
		fmt.Printf("Multiplies %d and %d into register %s.  Final value: %d\n", v1, v2, param1, p.registers[param1])
	}
}

func (p *program) jump(param1, param2 string) {
	// return one less than wanted, since the main loop has an increment of currentInstruction
	v1 := p.getAsNumber(param1)
	v2 := p.getAsNumber(param2)

	if v1 != 0 {
		p.currentInstruction += v2
		p.currentInstruction--
		if debug {
			fmt.Printf("Jump %d steps since parameter was %d\n", v2, v1)
		}
	} else {
		if debug {
			fmt.Printf("Parameter is 0, do nothing \n")
		}
	}
}

func (p *program) getAsNumber(param string) int {
	value, err := strconv.Atoi(param)

	if err != nil {
		value = p.registers[param]
	}
	return value
}

func getInput() []string {
	ret := []string{}
	fullFile, _ := ioutil.ReadFile(filename)
	for _, s := range strings.Split(string(fullFile), "\n") {
		trimmed := strings.Trim(s, "\r")
		trimmed = strings.Trim(trimmed, " ")
		if trimmed == "" {
			continue
		}
		ret = append(ret, strings.Trim(trimmed, "\t"))
	}
	return ret
}
